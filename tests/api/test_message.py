import pytest

from rest_framework import status

from mailings.models import Message


@pytest.mark.django_db
class TestMessage:
    def test_create_message(self, auth_user, mailing_instance, client_instance):
        payload = {
            'mailing': mailing_instance.id,
            'client': client_instance.id,
        }

        response = auth_user.post('/api/v1/message/', data=payload, format='json')
        data = response.data

        status_from_db = Message.objects.all().first()

        assert response.status_code == status.HTTP_201_CREATED
        assert data['mailing'] == status_from_db.mailing.id
        assert data['id'] == status_from_db.id

    def test_get_messages(self, auth_user, mailing_instance, client_instance):
        message = Message.objects.create(
            mailing=mailing_instance,
            client=client_instance,
        )

        response = auth_user.get('/api/v1/message/')

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data['results']) == 1

    def test_get_message_detail(self, auth_user, mailing_instance, client_instance):
        message = Message.objects.create(
            mailing=mailing_instance,
            client=client_instance,
        )

        response = auth_user.get(f'/api/v1/message/{message.id}/')
        data = response.data

        assert response.status_code == status.HTTP_200_OK
        assert data['id'] == message.id
        assert data['mailing'] == message.mailing.id

    def test_put_message(self, auth_user, mailing_instance, client_instance, mailing_instance_update, client_instance_update):
        message = Message.objects.create(
            mailing=mailing_instance,
            client=client_instance,
        )

        payload = {
            'mailing': mailing_instance_update.id,
            'client': client_instance_update.id,
        }

        response = auth_user.put(f'/api/v1/message/{message.id}/', payload)

        message.refresh_from_db()
        data = response.data

        assert data['id'] == message.id
        assert message.mailing.id == payload['mailing']

    def test_delete_message(self, auth_user, mailing_instance, client_instance):
        message = Message.objects.create(
            mailing=mailing_instance,
            client=client_instance,
        )

        response = auth_user.delete(f'/api/v1/message/{message.id}/')

        assert response.status_code == status.HTTP_204_NO_CONTENT

        with pytest.raises(Message.DoesNotExist):
            message.refresh_from_db()