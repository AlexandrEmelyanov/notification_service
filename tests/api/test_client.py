import pytest

from rest_framework import status

from users.models import Client


@pytest.mark.django_db
class TestClient:
    def test_create_client(self, auth_user):
        payload = {
            'phone_number': '79228683292',
            'operator_code': '922',
            'tag': 'new',
            'timezone': 'Asia/Yekaterinburg'
        }

        response = auth_user.post('/api/v1/client/', data=payload, format='json')
        data = response.data

        status_from_db = Client.objects.all().first()

        assert response.status_code == status.HTTP_201_CREATED
        assert data['phone_number'] == status_from_db.phone_number
        assert data['id'] == status_from_db.id

    def test_get_client_detail(self, auth_user):
        client = Client.objects.create(
            phone_number='79228683292',
            operator_code='922',
            tag='new',
            timezone='Asia/Yekaterinburg'
        )

        response = auth_user.get(f'/api/v1/client/{client.id}/')
        data = response.data

        assert response.status_code == status.HTTP_200_OK
        assert data['id'] == client.id
        assert data['phone_number'] == client.phone_number

    def test_get_clients(self, auth_user):
        client = Client.objects.create(
            phone_number='79228683292',
            operator_code='922',
            tag='new',
            timezone='Asia/Yekaterinburg'
        )

        response = auth_user.get('/api/v1/client/')

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data['results']) == 1

    def test_put_client(self, auth_user):
        client = Client.objects.create(
            phone_number='79228683292',
            operator_code='922',
            tag='new',
            timezone='Asia/Yekaterinburg'
        )

        payload = {
            'phone_number': '79428683292',
            'operator_code': '942',
            'tag': 'not new',
            'timezone': 'Asia/Yekaterinburg'
        }

        response = auth_user.put(f'/api/v1/client/{client.id}/', payload)

        client.refresh_from_db()
        data = response.data

        assert data['id'] == client.id
        assert client.phone_number == payload['phone_number']

    def test_delete_client(self, auth_user):
        client = Client.objects.create(
            phone_number='79228683292',
            operator_code='922',
            tag='new',
            timezone='Asia/Yekaterinburg'
        )

        response = auth_user.delete(f'/api/v1/client/{client.id}/')

        assert response.status_code == status.HTTP_204_NO_CONTENT

        with pytest.raises(Client.DoesNotExist):
            client.refresh_from_db()
