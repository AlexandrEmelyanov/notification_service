import pytest
from datetime import timedelta

from django.utils.timezone import now
from rest_framework import status

from mailings.models import Mailing


@pytest.mark.django_db
class TestMailing:
    def test_create_mailing(self, auth_user):
        payload = {
            'start_data': now(),
            'end_data': now() + timedelta(days=1),
            'content': 'for tag new',
            'tag': 'new',
            'operator_code': ''
        }

        response = auth_user.post('/api/v1/mailing/', data=payload, format='json')
        data = response.data

        status_from_db = Mailing.objects.all().first()

        assert response.status_code == status.HTTP_201_CREATED
        assert data['tag'] == status_from_db.tag
        assert data['id'] == status_from_db.id

    def test_get_mailings(self, auth_user):
        mailing = Mailing.objects.create(
            start_data=now(),
            end_data=now() + timedelta(days=1),
            content='for tag new',
            tag='new',
            operator_code=''
        )

        response = auth_user.get('/api/v1/mailing/')

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data['results']) == 1

    def test_get_mailing_detail(self, auth_user):
        mailing = Mailing.objects.create(
            start_data=now(),
            end_data=now() + timedelta(days=1),
            content='for tag new',
            tag='new',
            operator_code=''
        )

        response = auth_user.get(f'/api/v1/mailing/{mailing.id}/')
        data = response.data

        assert response.status_code == status.HTTP_200_OK
        assert data['id'] == mailing.id
        assert data['content'] == mailing.content

    def test_put_mailing(self, auth_user):
        mailing = Mailing.objects.create(
            start_data=now(),
            end_data=now() + timedelta(days=1),
            content='for tag new',
            tag='new',
            operator_code=''
        )

        payload = {
            'start_data': now() + timedelta(minutes=10),
            'end_data': now() + timedelta(days=2),
            'content': 'for tag new, update',
            'tag': 'not new',
            'operator_code': '951'
        }

        response = auth_user.put(f'/api/v1/mailing/{mailing.id}/', payload)

        mailing.refresh_from_db()
        data = response.data

        assert data['id'] == mailing.id
        assert mailing.tag == payload['tag']

    def test_delete_mailing(self, auth_user):
        mailing = Mailing.objects.create(
            start_data=now(),
            end_data=now() + timedelta(days=1),
            content='for tag new',
            tag='new',
            operator_code=''
        )

        response = auth_user.delete(f'/api/v1/mailing/{mailing.id}/')

        assert response.status_code == status.HTTP_204_NO_CONTENT

        with pytest.raises(Mailing.DoesNotExist):
            mailing.refresh_from_db()