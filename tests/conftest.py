import pytest
from datetime import timedelta

from django.utils.timezone import now
from django.contrib.auth.models import User
from rest_framework.test import APIClient

from users.models import Client
from mailings.models import Mailing


@pytest.fixture
def user():
    username = 'admin'
    password = 'admin'
    email = 'admin@example.com'

    user = User.objects.create_superuser(username, email, password)
    return user


@pytest.fixture
def client(django_db_reset_sequences):
    return APIClient()


@pytest.fixture
def auth_user(user, client):
    client.post('/api/v1/auth-admin/login/', dict(username=user.username, password='admin'))
    return client


@pytest.fixture
def mailing_instance():
    """For testing: TestMessage"""
    return Mailing.objects.create(
        start_data=now(),
        end_data=now() + timedelta(days=1),
        content='for tag new',
        tag='new',
        operator_code=''
    )


@pytest.fixture
def client_instance():
    """For testing: TestMessage"""
    return Client.objects.create(
        phone_number='79228683292',
        operator_code='922',
        tag='new',
        timezone='Asia/Yekaterinburg'
    )


@pytest.fixture
def mailing_instance_update():
    """For testing: test_put_message"""
    return Mailing.objects.create(
        start_data=now() + timedelta(minutes=10),
        end_data=now() + timedelta(days=3),
        content='for tag no tag',
        tag='no tag',
        operator_code=''
    )


@pytest.fixture
def client_instance_update():
    """For testing: test_put_message"""
    return Client.objects.create(
        phone_number='71228683292',
        operator_code='122',
        tag='no tag',
        timezone='Asia/Yekaterinburg'
    )
