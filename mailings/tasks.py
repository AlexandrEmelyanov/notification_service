import datetime
import logging
from datetime import datetime as dt
from datetime import timedelta

import pytz
import requests
from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail
from django.utils import timezone

from .models import Mailing, Message

TOKEN = settings.TOKEN
TEST_URL = settings.TEST_URL

logger = logging.getLogger(__name__)


@shared_task(bind=True)
def send_message_to_client(self, data: dict, mailing_id: int, url=TEST_URL, token=TOKEN) -> None:
    """
    Sending a message to an external service (TEST_URL).
    Processing of status change depending on the result of external service response (Undelivered or Delivered).

    If an error occurs when sending the request, it will be resent until the current
    time is greater than the mailing completion time.
    """
    message = Message.objects.filter(id=data['id']).first()
    end_date = Mailing.objects.filter(id=mailing_id).first().end_data
    header = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json',
    }

    if dt.now(pytz.utc) <= end_date:

        try:
            requests.post(url=url + str(message.id), headers=header, json=data)

        except requests.exceptions.RequestException as e:
            message.status = 'Undelivered'
            message.save()
            logger.error(f'Message to client {message.client.phone_number} undeliverable Status changed to Undelivered')
            raise self.retry(exc=e, countdown=3)

        else:
            message.status = 'Delivered'
            message.save()
            logger.info(f'Message to client {message.client.phone_number} delivered. Status changed to Delivered')

    else:
        logger.info(f'Mailing time is over. Message to client {message.client.phone_number} undeliverable')


@shared_task
def send_statistics() -> None:
    """Sending daily statistics to mail. The time of sending is 8 a.m. (settings.CELERY_BEAT_SCHEDULE)."""
    yesterday = timezone.now() - timedelta(days=1)
    subject = f'Daily mailing statistics for {yesterday.strftime("%d-%m-%Y")}'
    statistics = get_statistics(yesterday)

    if statistics:
        message = statistics
    else:
        message = 'There was no mailing for yesterday'

    send_mail(
        subject=subject,
        message=f'Statistics:\n{message}',
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[settings.EMAIL_HOST_USER],
        fail_silently=False,
    )

    logger.info(f"Statistics for {yesterday.strftime('%d-%m-%Y')} sent to mail")


def get_statistics(date: datetime) -> dict:
    """Generates statistics for the previous day. If there was no mailing on that day return -> {}."""
    mailings = Mailing.objects.filter(start_data__date=date)
    results = {}

    if mailings:
        statistics = []

        for mailing in mailings:
            total_messages = Message.objects.filter(mailing=mailing.id).count()
            delivered_messages = Message.objects.filter(mailing=mailing.id, status='Delivered').count()
            undelivered_messages = Message.objects.filter(mailing=mailing.id, status='Undelivered').count()

            mailing_info = {
                'id': mailing.id,
                'total_messages': total_messages,
                'delivered_messages': delivered_messages,
                'undelivered_messages': undelivered_messages
            }

            statistics.append(mailing_info)

        results = {
            'Total number of mailings': mailings.count(),
            'Statistics': statistics
        }

    return results
