from django.contrib import admin

from .models import Mailing, Message


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = (
        'start_data',
        'end_data',
        'tag',
        'operator_code',
        'total_messages',
        'delivered_messages',
        'undelivered_messages'
    )
    search_fields = ('start_data', 'tag', 'end_data', 'operator_code')
    ordering = ('-start_data',)
    list_per_page = 10
    list_filter = ('start_data', 'end_data')

    @admin.display(ordering='-start_data', description='Total sent messages')
    def total_messages(self, obj):
        """Getting statistics on the number of sent messages."""
        total = Message.objects.filter(mailing_id=obj.id).count()
        return total

    @admin.display(ordering='-start_data', description='Delivered messages')
    def delivered_messages(self, obj):
        """Getting statistics on the number of delivered messages."""
        delivered = Message.objects.filter(mailing_id=obj.id, status='Delivered').count()
        return delivered

    @admin.display(ordering='-start_data', description='Undelivered messages')
    def undelivered_messages(self, obj):
        """Getting statistics on the number of undelivered messages."""
        undelivered = Message.objects.filter(mailing_id=obj.id, status='Undelivered').count()
        return undelivered


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('created', 'status', 'mailing', 'client')
    search_fields = ('mailing', 'status', 'client')
    ordering = ('mailing',)
    list_editable = ('status',)
    list_per_page = 10
    list_filter = ('status',)
