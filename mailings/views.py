import logging

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count, F, Q
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Mailing, Message
from .paginations import APIListPagination
from .permissions import IsAdminOrReadOnly
from .serializers import MailingSerializer, MessageSerializer

logger = logging.getLogger(__name__)


class MessageViewSet(viewsets.ModelViewSet):
    """Additional option to create messages manually."""
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    pagination_class = APIListPagination
    permission_classes = (IsAdminOrReadOnly,)

    def perform_create(self, serializer):
        """Method override to log the creation of a message."""
        instance = serializer.save()
        logger.info(f'Message created: {instance}')

    def perform_update(self, serializer):
        """Method override to log the update of a message."""
        instance = serializer.save()
        logger.info(f'Message updated: {instance}')

    def perform_destroy(self, instance):
        """Method override to log the deletion of a message."""
        logger.info(f'Message deleted: {instance}')
        instance.delete()


class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    pagination_class = APIListPagination
    permission_classes = (IsAdminOrReadOnly,)

    def perform_create(self, serializer):
        """Method override to log the creation of a mailing."""
        instance = serializer.save()
        logger.info(f'Mailing created: {instance}')

    def perform_update(self, serializer):
        """Method override to log the update of a mailing."""
        instance = serializer.save()
        logger.info(f'Mailing updated: {instance}')

    def perform_destroy(self, instance):
        """Method override to log the deletion of a mailing."""
        logger.info(f'Mailing deleted: {instance}')
        instance.delete()


class DetailMailingStatsAPIView(APIView):
    def get(self, request, pk: int):
        results = {}

        try:
            Mailing.objects.get(pk=pk)

        except ObjectDoesNotExist:
            results = {
                'message': f'Object {pk} does not exists'
            }
        else:
            total_messages = Message.objects.filter(mailing=pk).count()
            tag = Mailing.objects.filter(pk=pk).values('tag').first()
            code = Mailing.objects.filter(pk=pk).values('operator_code').first()
            stats = Message.objects.filter(mailing=pk).values('status').annotate(count=Count('status'))

            results = {
                'Total messages': total_messages,
                'Filters': {'tag': tag['tag'], 'operator_code': code['operator_code']},
                'Statistic': stats
            }

        finally:
            return Response(results)


class GeneralStatsMailingsAPIView(APIView):
    def get(self, request):
        total_mailings = Mailing.objects.count()
        stats = Mailing.objects.annotate(
            total_messages=Count('message'),
            delivered_messages=Count('message', filter=Q(message__status='Delivered')),
            undelivered_messages=Count('message', filter=Q(message__status='Undelivered')),
            for_tag=F('tag'),
            for_operator_code=F('operator_code'),
        ).order_by('-id').values('id', 'total_messages', 'delivered_messages', 'undelivered_messages',
                                 'for_tag', 'for_operator_code')

        results = {
            'Total number of mailings': total_mailings,
            'Statistics': stats
        }

        return Response(results)
