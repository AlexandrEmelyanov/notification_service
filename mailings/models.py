from django.db import models

from users.models import Client


class Mailing(models.Model):
    start_data = models.DateTimeField(verbose_name='Start data of mailing')
    end_data = models.DateTimeField(verbose_name='End data of mailing')
    content = models.TextField(verbose_name='Message text')
    tag = models.CharField(verbose_name='Identification tag', max_length=128, blank=True)
    operator_code = models.CharField(verbose_name='Mobile operator code', max_length=3, blank=True)

    def __str__(self):
        return f'Mailing {self.id} from {self.start_data}'

    class Meta:
        verbose_name = 'Mailing'
        verbose_name_plural = 'Mailings'
        ordering = ('-id',)


class Message(models.Model):
    NOT_SENT = 'Not sent'
    SENT = 'Sent'
    DELIVERED = 'Delivered'
    UNDELIVERED = 'Undelivered'
    STATUSES = (
        (NOT_SENT, 'Not sent'),
        (SENT, 'Sent'),
        (DELIVERED, 'Delivered'),
        (UNDELIVERED, 'Undelivered'),
    )

    created = models.DateTimeField(verbose_name='Sending date', auto_now_add=True)
    status = models.CharField(verbose_name='Sending status', default=NOT_SENT, choices=STATUSES)
    mailing = models.ForeignKey(verbose_name='Mailing', to=Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(verbose_name='Client', to=Client, on_delete=models.CASCADE)

    def __str__(self):
        return f'Message {self.id} for client #{self.client.id}'

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
        ordering = ('-id',)
