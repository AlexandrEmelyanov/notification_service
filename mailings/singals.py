import logging
from datetime import datetime

import pytz
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import Client

from .models import Mailing, Message
from .tasks import send_message_to_client

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Mailing)
def created_mailing(sender: Mailing, instance: Mailing, created: bool, **kwargs: dict) -> None:
    """
    Event processing: creating a mailing.

    After the mailing is created, all clients matching the mailing filter are selected.
    Instances of the message are created. Based on the result of checking the start and end times of the mailing,
    the messages are sent to the clients (External service). Otherwise, it is postponed until the mailing start time.
    """
    if created:
        mailing = Mailing.objects.get(id=instance.id)
        mailing_id = mailing.id
        clients = Client.objects.filter(Q(tag=mailing.tag) | Q(operator_code=mailing.operator_code))

        for client in clients:
            message = Message.objects.create(mailing_id=mailing.id, client_id=client.id)

            data = {
                'id': message.id,
                'phone': client.phone_number,
                'content': mailing.content,
            }

            if check_date(instance=mailing):
                send_message_to_client.apply_async((data, mailing_id), expires=mailing.end_data, )
                message.status = 'Sent'
                message.save()
                logger.info(f'Message for {message.client.id} has been sent. Status changed to Sent')

            else:
                send_message_to_client.apply_async(
                    (data, mailing_id),
                    eta=mailing.start_data,
                    expires=mailing.end_data,
                )
                logger.info(f'Sending of the message for {message.client.id} postponed to {mailing.start_data}')


def check_date(instance: Mailing):
    """Checking the current time with the mailing time."""
    if instance.start_data <= datetime.now(pytz.utc) <= instance.end_data:
        return True
    return False
