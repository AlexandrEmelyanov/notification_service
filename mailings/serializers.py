from rest_framework import serializers

from .models import Mailing, Message


class MailingSerializer(serializers.ModelSerializer):
    start_data = serializers.DateTimeField(format='%d-%m-%Y %H:%M:%S')
    end_data = serializers.DateTimeField(format='%d-%m-%Y %H:%M:%S')

    class Meta:
        model = Mailing
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    created = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = '__all__'
        read_only_fields = ('status',)

    def get_created(self, obj: Message):
        return obj.created.strftime('%d-%m-%Y %H:%M:%S')
