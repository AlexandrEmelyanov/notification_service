from django.urls import include, path
from rest_framework import routers

from . import views

app_name = 'mailings_api'

router = routers.SimpleRouter()
router.register(r'message', views.MessageViewSet)
router.register(r'mailing', views.MailingViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('stat/', views.GeneralStatsMailingsAPIView.as_view()),
    path('stat/<int:pk>', views.DetailMailingStatsAPIView.as_view()),
]
