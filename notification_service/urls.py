from django.contrib import admin
from django.urls import include, path

from .yasg import urlpatterns as api_docs_url

urlpatterns = [
    path('admin/', admin.site.urls),

    # auth
    path('api/v1/auth-admin/', include('rest_framework.urls')),

    # api
    path('api/v1/', include('users.urls', namespace='client_api')),
    path('api/v1/', include('mailings.urls', namespace='mailings_api')),
]

# Addition API documentation
urlpatterns += api_docs_url
