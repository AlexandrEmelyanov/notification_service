from django.urls import include, path
from rest_framework import routers

from .views import ClientViewSet

app_name = 'client_api'

router = routers.SimpleRouter()
router.register(r'client', ClientViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
