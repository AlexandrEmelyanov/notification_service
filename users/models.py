from django.db import models

from .validators import validate_phone_number


class Client(models.Model):
    TIMEZONES = (
        ('Europe/Kaliningrad', 'Europe/Kaliningrad'),
        ('Europe/Moscow', 'Europe/Moscow'),
        ('Europe/Samara', 'Europe/Samara'),
        ('Asia/Yekaterinburg', 'Asia/Yekaterinburg'),
        ('Asia/Omsk', 'Asia/Omsk'),
        ('Asia/Novosibirsk', 'Asia/Novosibirsk'),
        ('Asia/Krasnoyarsk', 'Asia/Krasnoyarsk'),
        ('Asia/Irkutsk', 'Asia/Irkutsk'),
        ('Asia/Vladivostok', 'Asia/Vladivostok'),
        ('Asia/Yakutsk', 'Asia/Yakutsk'),
        ('Asia/Magadan', 'Asia/Magadan'),
        ('Asia/Kamchatka', 'Asia/Kamchatka'),
    )

    phone_number = models.CharField(
        verbose_name='Phone number',
        max_length=11,
        unique=True,
        validators=[validate_phone_number],
    )
    operator_code = models.CharField(verbose_name='Mobile operator code', max_length=3)
    tag = models.CharField(verbose_name='Identification tag', max_length=128)
    timezone = models.CharField(verbose_name='Time zone', max_length=64, choices=TIMEZONES)

    def __str__(self):
        return f'Client {self.id}, phone number: {self.phone_number}'

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'
        ordering = ('-id',)
