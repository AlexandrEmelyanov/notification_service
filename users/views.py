import logging

from rest_framework import status, viewsets
from rest_framework.response import Response

from mailings.permissions import IsAdminOrReadOnly

from .models import Client
from .paginations import ClientAPIListPagination
from .serializers import ClientSerializer

logger = logging.getLogger(__name__)


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    pagination_class = ClientAPIListPagination
    permission_classes = (IsAdminOrReadOnly,)

    def perform_create(self, serializer):
        """Method override to log the creation of a client."""
        logger.info(f"Creating a new client: {serializer.validated_data.get('phone_number')}")
        serializer.save()

    def perform_update(self, serializer):
        """Method override to log the update of a client."""
        logger.info(f'Updating client with id #{self.get_object().id}')
        serializer.save()

    def perform_destroy(self, instance):
        """Method override to log the deletion of a client."""
        logger.info(f'Deleting client with id #{instance.id}')
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
