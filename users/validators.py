from django.core.exceptions import ValidationError


def validate_phone_number(value):
    if not str(value).startswith('7'):
        raise ValidationError('Phone number must start with 7. Format: 7XXXXXXXXXX (X - digit from 0 to 9)')
