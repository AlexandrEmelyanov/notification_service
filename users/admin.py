from django.contrib import admin

from .models import Client


@admin.register(Client)
class Client(admin.ModelAdmin):
    list_display = ('id', 'phone_number', 'timezone', 'tag')
    search_fields = ('phone_number', 'timezone')
    ordering = ('id',)
    list_editable = ('phone_number', 'timezone')
    list_per_page = 10
    list_filter = ('timezone',)
